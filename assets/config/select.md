
## using literals

curl -s https://gitlab.com/oliver_michels_bechtle/public/-/raw/main/assets/config/all.json?ref_type=heads \
| jq ' .[] | select(.stage == "QA" and .name =="tomcat") '


## Using jq --arg

STAGE=DEV curl -s https://gitlab.com/oliver_michels_bechtle/public/-/raw/main/assets/config/all.json?ref_type=heads \
 | jq --arg jq_stage $STAGE ' .[] | select(.stage == $ARGS.named.jq_stage and .name =="tomcat") '

## using jq env

STAGE=DEV curl -s https://gitlab.com/oliver_michels_bechtle/public/-/raw/main/assets/config/all.json?ref_type=heads  \
| jq  ' .[] | select(.stage == env.STAGE and .name =="tomcat") '
